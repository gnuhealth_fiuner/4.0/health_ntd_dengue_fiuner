from trytond.wizard import Wizard, StateView, Button, StateTransition, StateAction
from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.pool import Pool 
from trytond.pyson import Eval, Not, Bool, Equal
from ..exceptions import EndDateBeforeStart

import csv
import sys
from io import StringIO
from datetime import datetime, date, time, timedelta


class CreateDengueReportsStart(ModelView):
    'Patient Following Report Start'
    __name__ = 'gnuhealth.dengue_du_survey.reports.start'

    report_ = fields.Selection([
        (None,''),
        ('create_dengue_location_report','Dengue Location Report'),
        ('create_dengue_reading_sheet_report','Dengue Reading Sheet Report'),
        ('create_dengue_weekly_report','Dengue Weekly Report'),
        ],'Report',required=True,sort=False)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)


class CreateDengueReportsWizard(Wizard):
    'Call Report Wizard'
    __name__ = 'gnuhealth.dengue_du_survey.reports.wizard'

    start = StateView('gnuhealth.dengue_du_survey.reports.start',
                      'health_ntd_dengue_fiuner.create_dengue_report_start_view',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Print Report','prevalidate','tryton-ok',default=True),
                       ])

    prevalidate = StateTransition()

    create_dengue_location_report =\
        StateAction('health_ntd_dengue_fiuner.act_gnuhealth_dengue_du_survey_location_report')

    create_dengue_reading_sheet_report =\
        StateAction('health_ntd_dengue_fiuner.act_gnuhealth_dengue_du_survey_reading_sheet_report')

    create_dengue_weekly_report =\
        StateAction('health_ntd_dengue_fiuner.act_gnuhealth_dengue_du_survey_weekly_report')

    def default_start(self,fields):
        return{
            'end_date':datetime.now()
            }

    def transition_prevalidate(self):
        if self.start.end_date < self.start.start_date:
            raise EndDateBeforeStart('end_date_before_start_date')
        return self.start.report_

    def fill_data(self):
        start = self.start.start_date
        end = self.start.end_date
        return {
            'start': start, 
            'end':end 
            }
        
    def do_create_dengue_location_report(self, action):
        data = self.fill_data()
        return action, data

    def do_create_dengue_reading_sheet_report(self, action):
        data = self.fill_data()
        return action, data
    
    def do_create_dengue_weekly_report(self, action):
        data = self.fill_data()
        return action, data
    
    
