from trytond.exceptions import UserError, UserWarning


class OvitrapInplaced(UserError):
    pass


class DuAlreadyHasOvitrap(UserWarning):
    pass


class EndDateBeforeStart(UserError):
    pass
