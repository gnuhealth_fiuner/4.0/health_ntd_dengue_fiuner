# -*- coding: utf-8 -*-

from trytond.pool import Pool
from trytond.report import Report

from datetime import datetime
from dateutil.relativedelta import relativedelta


class DengueLocationReport(Report):
    'Geo Referentiation Report'
    __name__ = 'gnuhealth.dengue_du_survey.location.report'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        DengueDuSurvey = pool.get('gnuhealth.dengue_du_survey')
        Config = Pool().get('gnuhealth.du.configuration')
        config = Config (1)
        
        context = super(DengueLocationReport, cls).get_context(records, data)
        
        start = data['start']
        end = data['end']
        
        
        context['today'] = datetime.now()
        context['city'] = config.default_du_address_city
        context['objects'] = DengueDuSurvey.search([
                                ('retirement_date','>=',start),
                                ('retirement_date','<=',end),
                                ('ovitrap','!=',None)])
        
        
        return context
        
 
