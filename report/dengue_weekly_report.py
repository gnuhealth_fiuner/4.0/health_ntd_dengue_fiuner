# -*- coding: utf-8 -*-

from trytond.pool import Pool
from trytond.report import Report

from datetime import datetime
from dateutil.relativedelta import relativedelta


class DengueWeeklyReport(Report):
    'Geo Referentiation Report'
    __name__ = 'gnuhealth.dengue_du_survey.weekly.report'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        DengueDuSurvey = pool.get('gnuhealth.dengue_du_survey')
        Config=Pool().get('gnuhealth.du.configuration')
        config = Config(1)
        context = super(DengueWeeklyReport, cls).get_context(records, data)

        start = context['start'] = data['start']
        end = context['end'] = data['end']
        context['city'] = config.default_du_address_city
        context['province'] = config.default_du_subdivision and config.default_du_subdivision.name

        dengue_du_survey = DengueDuSurvey.search([
                            ('placement_date','>=',start),
                            ('placement_date','<=',end),
                            ('ovitrap','!=',None)
                            ])
        epidemiological_week =\
            [str(x.epidemiological_week) for x in dengue_du_survey if x.epidemiological_week != None]
        #print('*'*20+'\n',epidemiological_week.sort())
        epidemiological_week.sort()
        context['epidemiological_week'] =\
            ', '.join(set(epidemiological_week))

        total = context['total_ovitraps_set'] = len(set(x.ovitrap.id for x in dengue_du_survey))
        actives = context['total_ovitraps_active'] =\
            len(set(x.ovitrap.id for x in dengue_du_survey if (x.first_read != None or x.second_read != None) and x.retirement_date != None))
        positives = context['total_ovitraps_positive'] =\
            len(set(x.ovitrap.id for x in dengue_du_survey if (x.first_read and x.first_read > 0)\
            or (x.second_read and x.second_read>0)))
        #OPI: Ovitramp Positive Index
        context['OPI'] = \
            positives / actives * 100 if actives > 0 else 0
        ovi_improve = context['ovitramps_code_where_to_improve'] = \
            ', '.join([x.ovitrap.name for x in dengue_du_survey if (x.aedes_larva or x.larva_in_house or x.larva_peri\
                or x.old_tyres or x.animal_water_container or x.flower_vase\
                or x.potted_plant or x.tree_holes or x.rock_holes) and x.ovitrap])
        context['DH'] = \
            sum([x.second_read or x.first_read for x in dengue_du_survey if x.second_read or x.first_read]) \
                / positives if positives > 0 else 0
        return context
